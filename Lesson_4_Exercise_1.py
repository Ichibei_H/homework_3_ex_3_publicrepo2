import math

a = 2.5
b = 5
if a == 0:
    a = a + 1
elif a < 0 or b < 0:
    print("a or b is negative")
    exit(0)
elif isinstance(a, float) or isinstance(b, float):
    a, b = math.ceil(a), math.ceil(b)
for x in range(a + 1, b + 1):
    a = x + a
print(a)
